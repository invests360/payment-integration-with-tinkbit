document.getElementById("bt_pay").addEventListener("click", function () {
  const s_token = btoa(
    "5e8c16995e3c3cf8aa6fb2aeb5d0b4b9d944b12f0d7211f190e462b027b1eb0d:c046dd019b11ff95c642393e93c86e91ed89f5e657f5672c1d880ce637e1d0a2"
  ); //BASIC AUTH TOKEN
  const s_url = "https://st-api.tinkbit.com/access/token";

  gen_token(s_url, s_token);
  async function gen_token(s_url, s_token) {
    var s_url = "https://st-api.tinkbit.com/access/token";
    var reponse = await fetch(s_url, {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Basic " + s_token,
      },
    });

    const parsed = await reponse.json();

    trans(parsed.data.access_token);
  }

  function trans(s_token) {
    // console.log(s_token);
    const references = document.getElementById("input").value;

    var customizationTBObject = {
      height: "100vh",
      width: "100%",
      "background-color": "white",
      border: "none",
    };

    var dataTB = {
      reference: references,
      fiat_amount: "12", //amount
      fiat_currency: "usd", //amount type
      crypto_currency: "ltc",
      first_name: "jon",
      middle_name: "jane",
      last_name: "doe",
      country: "United States",
      state: "California",
      city: "Los Angeles",
      address: "Street 48-A Molly building",
      dob: "2000-10-19",
      email: "jondoe@gmail.com",
      contact_number: "+923224448888",
      webhook_url: "https://example.com",//error page
      redirect_url: "https://example.com",//success page ??
    };

    var TB = new ApiCall(s_token, dataTB, customizationTBObject);
    TB.init();

    fetch('https://st-api.tinkbit.com/init/payment/transaction',
    {
      method : 'post',
      headers : {
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json',
        'Authorization' : 'Basic ' + s_token
      }
    })
    .then(function(response) {
         return response.json();
    }).then(function(data) { console.log(data); })

  }
});
